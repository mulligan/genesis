require 'test_helper'

class NotifierTest < ActionMailer::TestCase
  test "multipart_alternative" do
    mail = Notifier.multipart_alternative
    assert_equal "Multipart alternative", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
