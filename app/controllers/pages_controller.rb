class PagesController < ApplicationController
  def home

  end

  def customer_info
    @title = "Customer Info"
  end

  def questionnaire
    @title = "Profile Questionnaire"
  end

  def mailer
    flash[:notice] = "Your questionnaire has been submitted. We will contact you once it has been reviewed."
    Notifier.multipart_alternative(params).deliver!
    redirect_to(root_path)
  end
end
