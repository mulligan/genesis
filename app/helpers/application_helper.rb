module ApplicationHelper
  def title
    base_name         = "Genesis Trucking Consultants"
    base_abbreviation = "GTC"
    @title ? "#{base_abbreviation} | #{@title}" : "#{base_name}"
  end
end
