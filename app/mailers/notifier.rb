class Notifier < ActionMailer::Base
  default from: "genesistruckingconsultants@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.multipart_alternative.subject
  #
  def multipart_alternative mail_info
    @mail_info = mail_info

    mail to: "paulton@aol.com", subject: "Transporter Questionaire Received"
  end
end
